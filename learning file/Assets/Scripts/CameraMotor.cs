using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour
{
    public float camSpeed;
    private Vector2 screenBounds;
    

    private void Start()
    {

        //getting screen boundaries in world view
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, -10));
    }

    //calls after Update and fixedUpdate
    private void LateUpdate()
    {
        //continous motion 
        transform.Translate(camSpeed * Time.deltaTime, 0, 0);


    }
}
