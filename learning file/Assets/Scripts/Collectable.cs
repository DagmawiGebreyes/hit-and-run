using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : Collidable
{
    protected bool collected;

    protected override void OnCollide(Collider2D coll)
    {
        //is it the player colliding? NOTE: would need to alter if we change to 2P
        if (coll.name == "Player")
            OnCollect();
    }


    protected virtual void OnCollect()
    {
        collected = true;
    }

}
