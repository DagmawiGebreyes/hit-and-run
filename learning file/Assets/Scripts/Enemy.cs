using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Mover
{
    // Experience
    public int xpValue = 500;

    
    public float triggerLength = 1; //aggro range
    public float chaseLength = 20; //lose aggro range
    private bool chasing;
    private bool collidingWithPlayer; //stop them from continuing colliding
    public Transform playerTransform; //   TODO: ADD TO GAME MANAGER 
    private Vector3 startingPosition;

    public ContactFilter2D filter;
    private BoxCollider2D hitbox;
    private Collider2D[] hits = new Collider2D[10];


    protected override void Start()
    {
        base.Start();
        //playerTransform = GameManager.instance.player.transform;
        startingPosition = transform.position;
        //hitbox = transform.GetChild(0).GetComponent<BoxCollider2D>();
    }

    private void FixedUpdate()
    {
        //checking if the player is in aggro range
        if (Vector3.Distance(playerTransform.position, startingPosition) < chaseLength)
        {
            if (Vector3.Distance(playerTransform.position, startingPosition) < triggerLength)
            {
                chasing = true;
            }
            if (chasing)
            {
                if (!collidingWithPlayer)
                {
                    UpdateMotor((playerTransform.position - transform.position).normalized);
                }    
            }
        }

        //not in aggro range
        else
        {
            UpdateMotor(startingPosition - transform.position);
            chasing = false;
        }





        //checking for collisions (similar code to base collisions)
        boxCollider.OverlapCollider(filter, hits);

        //only continuing to OnCollide if i is a valid collider
        bool PlayerFound = false;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;

            if (hits[i].tag == "Fighter" && hits[i].name == "Player")
            {
                collidingWithPlayer = true;
                PlayerFound = true;
            }


            //clean up array
            hits[i] = null;
        }

        //otherwise enemy just stands there after first collision
        if (!PlayerFound)
        {
            collidingWithPlayer = false;
        }


    }


    protected override void Death()
    {
        Destroy(this.gameObject);
        GameManager.instance.score += xpValue;

    }
    
}
