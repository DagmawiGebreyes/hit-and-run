using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public AudioSource Swing;
    public AudioSource Stab;
    public AudioSource Coin;

    public void PlaySwing()
    {
        Swing.Play();
    }

    public void PlayStab()
    {
        Stab.Play();
    }

    public void PlayCoin()
    {
        Coin.Play();
    }
}
