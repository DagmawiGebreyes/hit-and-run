using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//currently using a 14x14 sprite
public class Player : Mover
{
    


    private void FixedUpdate()
    {
        //returns -1 if you're holding a/left, 0 if no keys, 1 if holding d/right
        //edit > project settings > Input: see the button links
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        UpdateMotor(new Vector3(x, y, 0));
    }


}




