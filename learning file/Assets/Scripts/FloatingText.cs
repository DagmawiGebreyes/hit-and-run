using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //import for Text

public class FloatingText
{
    public bool active; //whether it's being used
    public GameObject go;
    public Text txt;

    public Vector3 motion;
    public float duration;
    public float lastShown;


    //show text
    public void Show()
    {
        active = true;
        lastShown = Time.time;
        go.SetActive(active);
    }


    //hide text
    public void Hide()
    {
        active = false;
        go.SetActive(active);
    }

    public void UpdateFloatingText()
    {
        if (!active)
            return;


        if(Time.time - lastShown > duration)
        {
            Hide();
        }

        //while active, every frame we move
        go.transform.position += motion * Time.deltaTime;
    }
}
