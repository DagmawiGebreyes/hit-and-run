using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//can't drag this on any object - don't need
public abstract class Mover : Fighter
{
    protected BoxCollider2D boxCollider;
    protected Vector3 moveDelta;
    protected RaycastHit2D hit;
    protected float ySpeed = 0.9f;
    protected float xSpeed = 1.0f;

    public float moveSpeed;


    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }



    protected virtual void UpdateMotor(Vector3 input)
    {
        moveDelta = new Vector3(input.x * xSpeed, input.y * ySpeed, 0);


        //sprite direction changing - changing scale to mirror
        if (moveDelta.x > 0)
            transform.localScale = Vector3.one; //new Vector3(1, 1, 1)
        else if (moveDelta.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);




        //collisions: casts a box where we about to move first, if box returns null we can move

        //y-axis
        //collisions: boxcast @ current position at angle 0, direction: y-axis, distance: absolute value of same movement distance, which layer are we testing against
        hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(0, moveDelta.y), Mathf.Abs(moveDelta.y * Time.deltaTime), LayerMask.GetMask("Actor", "Blocking"));
        if (hit.collider == null)
        {
            //movement - equal time on all devices w deltaTime
            transform.Translate(0, moveDelta.y * (Time.deltaTime * moveSpeed), 0);
        }

        //x-axis
        //collisions: boxcast @ current position at angle 0, direction: y-axis, distance: absolute value of same movement distance, which layer are we testing against
        hit = Physics2D.BoxCast(transform.position, boxCollider.size, 0, new Vector2(moveDelta.x, 0), Mathf.Abs(moveDelta.x * Time.deltaTime), LayerMask.GetMask("Actor", "Blocking"));
        if (hit.collider == null)
        {
            //movement - equal time on all devices w deltaTime
            transform.Translate(moveDelta.x * (Time.deltaTime * moveSpeed), 0, 0);
        }
    }
}
