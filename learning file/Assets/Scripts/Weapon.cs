using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Collidable
{
    //Damage structure
    public int damagePoint = 1;
    public float pushForce = 2.0f;

    // Upgrade
    public int weaponLevel = 0;
    private SpriteRenderer spriteRenderer;


    //Attack
    private float cooldown = 0.5f;
    private float lastSwing;
    private Animator anim;

    public AudioSource SwingSound;



    protected override void Start()
    {
        base.Start();

        //so we can switch out weapon sprite later
        spriteRenderer = GetComponent<SpriteRenderer>();

        anim = GetComponent<Animator>();
    }


    protected override void Update()
    {
        base.Update();

        //giving the attack cd
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(Time.time - lastSwing > cooldown)
            {
                lastSwing = Time.time;
                Swing();
            }
        }
    }


    // what are we hitting
    protected override void OnCollide(Collider2D coll)
    {
        if (coll.tag == "Enemy")
        { 
            //sending a new damage object to hitted enemy
            Damage dmg = new Damage
            {
                damageAmount = damagePoint,
                origin = transform.position, //origin = player
                pushForce = pushForce
            };

            coll.SendMessage("ReceiveDamage", dmg);
        }


    }

    private void Swing()
    {
        SwingSound.Play();
        anim.SetTrigger("Swing");
    }
}
