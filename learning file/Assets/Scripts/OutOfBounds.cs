using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    public Transform playerLoc;
    private float deltaX;


    // Update is called once per frame
    private void LateUpdate()
    {
        //checking if player out of bounds and gives warning first

        deltaX = playerLoc.position.x - transform.position.x;
        if (deltaX < 0)
        {
            print("game over");
        }
        //warning allowance
        else if (deltaX < 0.1)
        {
            print("danger");
        }

    }
}
