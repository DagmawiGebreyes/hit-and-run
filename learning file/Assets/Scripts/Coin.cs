using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : Collectable
{
    private int value = 3;
    public AudioSource CoinSound;

    protected override void OnCollect()
    {

        if(!collected)
        {
            Destroy(this.gameObject);
            GameManager.instance.money += value;
            CoinSound.Play();
        }

        //collected = true;

    }

}
